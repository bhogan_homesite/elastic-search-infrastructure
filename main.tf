provider "aws" {
  version = "~> 2.0"
  region  = "us-east-1"
}

resource "aws_iam_role" "bhogan-elastic-search-function-role2" {
  name = "bhogan-elastic-search-function-role2"
  path = "/service-role/"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}


resource "aws_iam_role_policy_attachment" "dynamodb_full_access_attach" {
  role       = aws_iam_role.bhogan-elastic-search-function-role2.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
}

resource "aws_iam_role_policy_attachment" "lambda_kinesis_execution_attach" {
  role       = aws_iam_role.bhogan-elastic-search-function-role2.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaKinesisExecutionRole"
}

resource "aws_iam_role_policy_attachment" "sns_full_access_attach" {
  role       = aws_iam_role.bhogan-elastic-search-function-role2.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSNSFullAccess"
}

resource "aws_kinesis_stream" "bhogan_elasticsearch_stream2" {
  name             = "bhogan-elasticsearch-stream2"
  shard_count      = 2
  retention_period = 24

  shard_level_metrics = [
    "IncomingBytes",
    "OutgoingBytes",
  ]
}


resource "aws_lambda_function" "bhogan_elastic_search_function2" {
  filename      = "lambda.zip"
  function_name = "bhogan-elastic-search-function2"
  role          = aws_iam_role.bhogan-elastic-search-function-role2.arn
  handler       = "index.handler"

  # The filebase64sha256() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the base64sha256() function and the file() function:
  # source_code_hash = "${base64sha256(file("lambda_function_payload.zip"))}"
  source_code_hash = filebase64sha256("lambda.zip")

  runtime = "nodejs12.x"

}

resource "aws_lambda_event_source_mapping" "bhogan_elasticsearch_stream2_trigger" {
  event_source_arn  = aws_kinesis_stream.bhogan_elasticsearch_stream2.arn
  function_name     = aws_lambda_function.bhogan_elastic_search_function2.arn
  starting_position = "LATEST"
  batch_size = 10
  maximum_retry_attempts = 1
  maximum_record_age_in_seconds = 60
}


variable "domain" {
  default = "bhogan-test04"
}

data "aws_region" "current" {}

data "aws_caller_identity" "current" {}

resource "aws_elasticsearch_domain" "bhogan_elasticsearch2" {
  domain_name           = var.domain
  elasticsearch_version = "7.7"

  ebs_options {
    ebs_enabled = true
    volume_type = "gp2"
    volume_size = 10
  }
  cluster_config {
    instance_type = "t2.medium.elasticsearch"
  }

  snapshot_options {
    automated_snapshot_start_hour = 00
  }

    access_policies = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "arn:aws:sts::673839138862:assumed-role/hs-home-sandbox-user/bhogan@homesite.com",
          "arn:aws:iam::673839138862:role/service-role/bhogan-elastic-search-function-role",
          "${aws_iam_role.bhogan-elastic-search-function-role2.arn}"
        ]
      },
      "Action": "es:*",
      "Resource": "arn:aws:es:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:domain/${var.domain}/*"
    },
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "*"
      },
      "Action": "es:*",
      "Resource": "arn:aws:es:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:domain/${var.domain}/*",
      "Condition": {
        "IpAddress": {
          "aws:SourceIp": [
            "104.129.194.197",
            "104.129.194.249",
            "199.189.178.1",
            "165.225.38.254",
            "108.26.161.28"
          ]
        }
      }
    }
  ]
}
POLICY
}

### to do, get the arn url of the elastic search into the lambda
